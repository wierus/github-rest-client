package pl.pstanek.githubrestclient.service;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;
import pl.pstanek.githubrestclient.model.Repository;
import pl.pstanek.githubrestclient.model.User;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RepositoryService {

    private static final String PREFIX_USER_REPOSITORIES = "https://api.github.com/users/";
    private static final String SUFFIX_USER_REPOSITORIES = "/repos";
    private static final String QUERY_PAGE_PARAM = "?page=";
    private static final String GITHUB_API_AUTHORIZATION_TOKEN = "token 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";

    public int getAllUsersStars(String username) {
        List<Repository> userRepositories = getUserRepositories(username);
        return userRepositories.stream()
                .map(Repository::getStars)
                .mapToInt(sg -> sg).sum();
    }

    public List<Repository> getUserRepositories(String username) {
        User user = new User();
        user.setUsername(username);
        String path = PREFIX_USER_REPOSITORIES + user.getUsername() + SUFFIX_USER_REPOSITORIES;
        Response response = callGithubApi(path);
        int pagesNumber = extractNumberOfPages(response);
        List<Repository> repositoryList = extractResponseBody(response);
        user.addRepositories(repositoryList);
        if (pagesNumber > 1) {
            traverseTroughPages(user, path, pagesNumber);
        }
        return user.getRepositories();
    }

    private void traverseTroughPages(User user, String path, int pagesNumber) {
        for (int i = 2; i <= pagesNumber; i++) {
            String pathToPage = path + QUERY_PAGE_PARAM + i;
            Response response = callGithubApi(pathToPage);
            List<Repository> repositoryList = extractResponseBody(response);
            user.addRepositories(repositoryList);
        }
    }

    private List<Repository> extractResponseBody(Response response) {
        Repository[] repos = null;
        Reader reader;
        if (response.body() != null) {
            reader = response.body().charStream();
            repos = new Gson().fromJson(reader, Repository[].class);
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        response.close();
        if (repos != null) {
            return new ArrayList<>(Arrays.asList(repos));
        } else return new ArrayList<>();
    }

    private Response callGithubApi(String fullPath) {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder()
                .url(fullPath)
                .method("GET", null)
                .addHeader("Authorization", GITHUB_API_AUTHORIZATION_TOKEN)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    private int extractNumberOfPages(Response response) {
        String links = response.header("Link");
        int pagesCount = 0;
        if (links != null && !links.isEmpty()) {
            String[] split = links.split(", ");
            for (String ref : split) {
                if (ref.contains("rel=\"last\"")) {
                    String httpsPattern = "((\\?page=)(\\d))";
                    Pattern pattern = Pattern.compile(httpsPattern);
                    Matcher matcher = pattern.matcher(ref);
                    while (matcher.find()) {
                        pagesCount = Integer.parseInt(matcher.group(3));
                    }
                }
            }
        }
        return pagesCount;
    }
}
