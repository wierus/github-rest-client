package pl.pstanek.githubrestclient.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.pstanek.githubrestclient.model.Repository;
import pl.pstanek.githubrestclient.service.RepositoryService;

import java.util.List;

@RestController
public class RepositoryController {

    private final RepositoryService repositoryService;

    public RepositoryController(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @GetMapping(path = "/github/users/{user}")
    public ResponseEntity<List<Repository>> getAllUserRepositories(@PathVariable("user") String username) {
        try {
            return ResponseEntity.ok(repositoryService.getUserRepositories(username));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/github/users/{user}/stars")
    public ResponseEntity<Integer> getSumUserStargazes(@PathVariable("user") String username) {
        try {
            return ResponseEntity.ok(repositoryService.getAllUsersStars(username));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
