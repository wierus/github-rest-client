package pl.pstanek.githubrestclient.model;

import java.util.ArrayList;
import java.util.List;

public class User {

    private List<Repository> repositories = new ArrayList<>();
    private int allStars = 0;
    private String username;

    public User() {
    }

    public User(List<Repository> repositories, int allStars) {
        this.repositories = repositories;
        this.allStars = allStars;
    }

    public void addRepositories(List<Repository> newRepositories) {
        this.repositories.addAll(newRepositories);
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAllStars() {
        return allStars;
    }

    public void setAllStars(int allStars) {
        this.allStars = allStars;
    }
}
