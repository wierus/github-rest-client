### Urochomienie aplikacji:
1. Sklonuj lub ściągnij repozytorium
2. Przejdź do głównego folderu (github-rest-client)
3. Przy pomocy wiersza poleceń uruchom `./mvnw spring-boot:run`
<br>
lub przy użyciu ulubionego IDE uruchom klasę github-rest-client/src/main/java/pl.pstanek.githubrestclient/GithubRestClientApplication

Aplikacja po uruchomieniu wystawia dwa endpointy GET:
<br>
`http://localhost:8080/github/users/{user}` - zwraca liste repozytoriów podanego użytkownia ({user}) w formacie json 
<br>
`http://localhost:8080/github/users/{user}/stars` - zwraca sumę gwiazdek ze wszystkich repozytoriów użytkownika ({user})
<br>

#### Możliwości rozszerzenia funkcji aplikacji:
- Dzięki dodaniu nowych funkcjonalności np. takich jak sprawdzanie poszczególnych branchy, zmiany uprawnień, przeglądanie commitów czy wyświetlanie i dodawanie komentarzy, można stworzyć narzędzie do zarządzania i przeglądania repozytoriów swoich jak i innych użytkowników.
- W celu poprawienia wydajności i czasu oczekiwania na odpowiedź pożądanym byłoby zastąpienie synchronicznego odpytywania Github API asynchronicznym
